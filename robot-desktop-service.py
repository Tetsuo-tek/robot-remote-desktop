#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.elapsedtime import ElapsedTime
from PySketch.abstractflow import FlowChannel
from PySketch.flowsync import FlowSync
from PySketch.flowproto import FlowChanID, Variant_T, Flow_T
from PySketch.flowsat import FlowSat

from PySketch.videoenc import PsyvEncoder

import time
import argparse

import cv2 as cv
import numpy as np

import mss
import pyautogui
import struct
import unicodedata

from pynput.keyboard import Controller
keyboard = Controller()

###############################################################################
# GLOBAL

sat = None

serviceName = "RemoteDesktop.Ctrl"
serviceChan = None
serviceUsed = False
serviceCtrlPack = None

videoOutChan = None
outEnabled = False

mousePosChan = None
mouseClkChan = None
keyboardChan = None

#chrono = ElapsedTime()

args = None

parser = argparse.ArgumentParser(description="desktopGrabber")
parser.add_argument('sketchfile', help='Sketch program file')
parser.add_argument('--user', help='Flow-network username', default='User1')
parser.add_argument('--password', help='Flow-network password', default='password')
parser.add_argument('--fps', help='Max fps for video stream', default='15')
parser.add_argument('--resize-factor', help='Divide original resolution for a reducing factor', default='1')
parser.add_argument('--compression', help='Video stream jpeg compression value (0, 100)', default='40')
parser.add_argument('--monitor-index', help='Index of the monitor to control (0 is All screen, > 0 are signle screens)', default='1')
parser.add_argument('--show-changes', help='Show Video stream frame changes countours for debug purposes', action='store_true')

pyautogui.FAILSAFE = False

tickInterval = 0
fps = 0
factor = 0
resizeW = 0
resizeH = 0
compression = 0

currentMouseX = 0
currentMouseY = 0

draggingOnLeft = False
draggingOnRight = False

sct = mss.mss(with_cursor=True)
monitorID = -1
monitor = None
monitorW = 0
monitorH = 0

encoder = None
compressionParams = None

#prevFrame = None

showChanges = False

###############################################################################
# SKETCH

def setup():
    global args
    global parser
    global sat
    global fps
    global tickInterval
    global monitorID
    global monitor
    global monitorW
    global monitorH
    global factor
    global resizeW
    global resizeH
    global compression
    global serviceName
    global showChanges
    global encoder
    global compressionParams

    args = parser.parse_args()

    sat = FlowSat()
    sat._userName = args.user
    sat._passwd = args.password

    sat.setRequestCallBack(onServiceRequest)
    sat.setNewChanCallBack(onChannelAdded)
    sat.setDelChanCallBack(onChannelRemoved)
    sat.setGrabDataCallBack(onDataGrabbed)
    sat.setStartChanPubReqCallBack(onPublishStartReq)
    sat.setStopChanPubReqCallBack(onPublishStopReq)
 
    showChanges = args.show_changes
    fps = int(args.fps)
    tickInterval = 1.0 / float(fps)

    sat.setTickTimer(tickInterval, tickInterval * 10)

    monitorID = int(args.monitor_index)
    monitor = sct.monitors[monitorID]
    factor = float(args.resize_factor)
    monitorW = monitor["width"]
    monitorH = monitor["height"]
    resizeW = int(float(monitorW) / factor)
    resizeH = int(float(monitorH) / factor)

    compression = int(args.compression)
    compressionParams = [cv.IMWRITE_JPEG_QUALITY, compression]

    print("Monitor index: {}".format(monitorID))
    print("Monitor resolution: {}x{}".format(monitorW, monitorH))
    print("Output resolution: {}x{}".format(resizeW, resizeH))
    print("Jpeg compression: {}".format(compression))
    print("FPS: {}".format(fps))
    print("Tick interval: {} s".format(tickInterval))
 
    ok =  sat.connect()

    if ok:
        print("Creating service ..")

        minBlkSide = 8
        encoder = PsyvEncoder(resizeW, resizeH, fps, minBlkSide)

        sat.addServiceChannel(serviceName)
        #sat.addStreamingChannel(Flow_T.FT_VIDEO_DATA, Variant_T.T_BYTEARRAY, "Screen.JPeg", "image/jpeg")
        sat.addStreamingChannel(Flow_T.FT_VIDEO_DATA, Variant_T.T_BYTEARRAY, "Screen.PSYV")

    return ok

def loop():
    global sat
    global sct
    global monitor
    global resizeW
    global resizeH
    global compression
    global videoOutChan
    global outEnabled
    global tickInterval
    global prevFrame

    if videoOutChan is None or not videoOutChan.isPublishingEnabled:
        sat.tick()
        return sat.isConnected()

    f = np.array(sct.grab(monitor))

    if factor != 1:
        f = cv.resize(f, (resizeW, resizeH), interpolation=cv.INTER_NEAREST)

    changed = encoder.encode(f)

    if len(changed) > 0:
        dataSegment = bytearray()

        for blk in changed:
            dataSegment.extend(encoder.produce(blk, compressionParams))

        sat.publish(videoOutChan.chanID, dataSegment)

    sat.tick()

    '''
    if videoOutChan is None or not outEnabled or chrono.stop() <=  tickInterval:
        return sat.isConnected()

    chrono.start()

    img = np.array(sct.grab(monitor))
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    send = False

    if prevFrame is None:
        send = True

    else:
        difference = cv.absdiff(prevFrame, gray)
        _, thresholded = cv.threshold(difference, 30, 255, cv.THRESH_BINARY)
        contours, _ = cv.findContours(thresholded, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        
        if showChanges:
            cv.drawContours(img, contours, -1, (0, 0, 255), 2)

        send = (len(contours) > 0)

    if not send:
        return sat.isConnected()

    prevFrame = np.copy(gray)

    if resizeW > 0:
        img = cv.resize(img, (resizeW, resizeH))

    compression_params = [cv.IMWRITE_JPEG_QUALITY, compression]
    success, encoded_image = cv.imencode('.jpg', img, compression_params)

    if not success:
        print("Image encoding error")

    data = bytearray(encoded_image)
    sat.publish(videoOutChan.chanID, data)
    '''

    return sat.isConnected()
    
###############################################################################
# CALLBACKs
        
def onServiceRequest(chanID, hash, cmdName, val):
    global sat
    global serviceChan
    global serviceCtrlPack

    if serviceChan.chanID == chanID:
        print("Service request RECEIVED [cmdName: {}; hash: {}]: {}".format(cmdName, hash, val))
        serviceCtrlPack = val["value"]
        print(serviceCtrlPack["mouseMove"])
        response = {
            "status" : True,
            "height" : monitorH,
            "width" : monitorW
        }
        sat.sendServiceResponse(chanID, hash, response)

def onChannelAdded(ch: FlowChannel):
    global sat
    global serviceName
    global serviceChan
    global serviceCtrlPack
    global videoOutChan
    global mousePosChan
    global mouseClkChan
    global keyboardChan
    global fps
    global resizeW
    global resizeH
    global tickInterval

    if ch.name == "{}.{}".format(sat._userName, serviceName):
        serviceChan = ch
        print("Service is READY: {}".format(serviceChan.name))

    #elif ch.name == "{}.Screen.JPeg".format(sat._userName):
    elif ch.name == "{}.Screen.PSYV".format(sat._userName):
        videoOutChan = ch

        sat.setCurrentDbName(videoOutChan.name)
        sat.setVariable("resolution", "{}x{}".format(resizeW, resizeH))
        sat.setVariable("fps", fps)
        sat.setVariable("frameRawSize", int(resizeW * resizeH * 3))
        sat.setVariable("tickTimePeriod", tickInterval)
        #sat.setVariable("stream", "MULTIPART")
        #sat.setVariable("codec", "JPEG")
        sat.setVariable("stream", "MGM")
        sat.setVariable("codec", "PSYV")
        sat.setCurrentDbName(sat._userName)

        print("Channel {} CREATED [ChanID: {}]".format(videoOutChan.name, videoOutChan.chanID))

    elif serviceCtrlPack is not None:
        if ch.name == serviceCtrlPack["mouseMove"]:
            mousePosChan = ch
            print("Channel {} FOUND [ChanID: {}]".format(mousePosChan.name, mousePosChan.chanID))

        elif ch.name == serviceCtrlPack["mouseClick"]:
            mouseClkChan = ch
            print("Channel {} FOUND [ChanID: {}]".format(mouseClkChan.name, mouseClkChan.chanID))

        elif ch.name == serviceCtrlPack["keyboard"]:
            keyboardChan = ch
            print("Channel {} FOUND [ChanID: {}]".format(keyboardChan.name, keyboardChan.chanID))

        else:
            return

        # here it can crash
        sat.subscribeChannel(ch.chanID)

def onChannelRemoved(ch):
    global sat
    global serviceName
    global serviceChan
    global serviceCtrlPack
    global mousePosChan
    global mouseClkChan
    global keyboardChan
    global videoOutChan

    #if serviceCtrlPack is None:
    #    return

    if ch.name == serviceCtrlPack["mouseMove"]: 
        print("Channel {} UNSUBSCRIBE [ChanID: {}]".format(mousePosChan.name, mousePosChan.chanID))
        mousePosChan = None

    elif ch.name == serviceCtrlPack["mouseClick"]:
        print("Channel {} UNSUBSCRIBE [ChanID: {}]".format(mouseClkChan.name, mouseClkChan.chanID))
        mouseClkChan = None

    elif ch.name == serviceCtrlPack["keyboard"]:
        print("Channel {} UNSUBSCRIBE [ChanID: {}]".format(keyboardChan.name, keyboardChan.chanID))
        keyboardChan = None

    elif ch.name == "{}.Screen.PSYV".format(sat._userName):
        videoOutChan = None

def onPublishStartReq(ch: FlowChannel):
    global outEnabled
    #global chrono

    #if ch.name == "{}.Screen.JPeg".format(sat._userName):
    if ch.name == "{}.Screen.PSYV".format(sat._userName):
        outEnabled = True
        #chrono.start()
        print("Activated video publish")

def onPublishStopReq(ch: FlowChannel):
    global outEnabled

    #if ch.name == "{}.Screen.JPeg".format(sat._userName):
    if ch.name == "{}.Screen.PSYV".format(sat._userName):
        outEnabled = False
        #prevFrame = None
        print("Deactivated video publish")

def onDataGrabbed(chanID, data):
    global sat
    global mousePosChan
    global mouseClkChan
    global keyboardChan
    global monitorW
    global monitorH
    global currentMouseX
    global currentMouseY
    global draggingOnLeft
    global draggingOnRight
    global hotKeyConvert

    if mousePosChan.chanID == chanID:
        posArray = struct.unpack('<HH', data)
        currentMouseX = int(posArray[0])
        currentMouseY = int(posArray[1])

        if currentMouseX >= monitorW or currentMouseY >= monitorH:
            return

        if currentMouseX == 0 or currentMouseY == 0:
            return

        #print("MOVE -> x: {}, y: {}".format(currentMouseX, currentMouseY))  
        pyautogui.moveTo(currentMouseX, currentMouseY, duration=0, _pause=False)
            
    elif mouseClkChan.chanID == chanID:
        val, = struct.unpack('<B', data)

        # LEFT

        if val == 0: # left click
            pyautogui.mouseDown(button='left')
            print("Received Left-CLICK -> {}".format(val))

        elif val == 1: # left double-click
            pyautogui.doubleClick(interval=0, button='left')
            print("Received Left-DoubleCLICK -> {}".format(val))

        elif val == 2: # left release
            print("Received Left-RELEASE -> {}".format(val))
            pyautogui.mouseUp(button='left')

        # CENTER

        elif val == 3: # center click
            pyautogui.mouseDown(button='middle')
            print("Received Center-CLICK -> {}".format(val))
            
        elif val == 4: # center double-click
            pyautogui.doubleClick(interval=0, button='middle')
            print("Received Center-DoubleCLICK -> {}".format(val))

        elif val == 5: # center release
            print("Received Center-RELEASE -> {}".format(val))
            pyautogui.mouseUp(button='middle')

        # RIGHT

        elif val == 6: # right click
            pyautogui.mouseDown(button='right')
            print("Received Right-CLICK -> {}".format(val))

        elif val == 7: # right double-click
            pyautogui.doubleClick(interval=0, button='right')
            print("Received Right-DoubleCLICK -> {}".format(val))

        elif val == 8: # right release
            print("Received Right-RELEASE -> {}".format(val))
            pyautogui.mouseUp(button='right')

        # SCROLL

        elif val == 9: # center release
            pyautogui.scroll(1)

        elif val == 10: # center release
            pyautogui.scroll(-1)

    elif keyboardChan.chanID == chanID:
        val = data.decode('utf-8')
        #print("Keyboard RECEIVED: {}".format(val))
        key = val[1:]

        if val.startswith('@') or val.startswith('#'):
            converted = hotKeyConvert.get(key)
        
            if converted is None:
                converted = key

            if val.startswith('@'):
                if len(key) == 1 or converted in onlyPress:
                    print("Key ATOMIC: {} -> {}".format(key, converted))
                    pyautogui.press(converted)
                    return

                else:
                    print("Key PRESSED: {} -> {}".format(key, converted))
                    pyautogui.keyDown(converted, _pause=False)

            elif val.startswith('#'):
                if len(key) == 1 or converted in onlyPress:
                    return

                print("Key RELEASED: {} -> {}".format(key, converted))
                pyautogui.keyUp(converted, _pause=False)

        elif val.startswith('!'):
            print("Key CHAR: {}".format(key))
            #pyautogui does NOT support dynamic keymapping 
            #pyautogui.press(key, _pause=False)
            #pyautogui.write(key, interval=0,_pause=False)
            keyboard.type(key)
            #pyautogui.typewrite(key)

            '''
            the second line is working if you use the KeyCode.from_char class instead:
                python3 -c "import pynput; pynput.keyboard.Controller().press(pynput.keyboard.KeyCode.from_char('a'))"
            but you can also use the plain character as argument to the press method:
                python3 -c "import pynput; pynput.keyboard.Controller().press('a')
            '''

# FROM Tk TO PyAutoGui

onlyPress = [
    "backspace",
    "delete",
    "space"
]

hotKeyConvert = {
    'Up': 'up',
    'Down': 'down',
    'Left': 'left',
    'Right': 'right',
    'Escape': 'esc',
    'Insert': 'insert',
    'Home': 'home',
    'End': 'end',
    'Next' : 'pageup',
    'Prior' : 'pagedown',
    'Tab': 'tab',
    'Scroll_Lock' : 'scrolllock',
    'Pause' : 'pause',
    'Num_Lock' : 'numlock',
    'Caps_Lock' : 'capslock',
    'BackSpace': 'backspace',
    'Delete': 'delete',
    'Return': 'enter',
    'F1': 'f1',
    'F2': 'f2',
    'F3': 'f3',
    'F4': 'f4',
    'F5': 'f5',
    'F6': 'f6',
    'F7': 'f7',
    'F8': 'f8',
    'F9': 'f9',
    'F10': 'f10',
    'F11': 'f11',
    'F12': 'f12',
    'Control_L': 'ctrlleft',
    'Control_R': 'ctrlright',
    'Shift_L': 'shiftleft',
    'Shift_R': 'shiftright',
    'Alt_L': 'altleft',
    #'ISO_Level3_Shift': 'altright',
    'Super_L': 'winleft',
    'Super_R': 'winright'
}

###############################################################################

