#!/usr/bin/env pysketch-executor


###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.elapsedtime import ElapsedTime
from PySketch.abstractflow import FlowChannel
from PySketch.flowsync import FlowSync
from PySketch.flowproto import FlowChanID, Variant_T, Flow_T
from PySketch.flowsat import FlowSat

from PySketch.videodec import PsyvDecoder

import numpy as np
import cv2 as cv

from queue import Queue
import tkinter as tk
#from pynput import keyboard
from PIL import Image, ImageTk
import threading
import io
import struct

###############################################################################
#SKETCH

sat = None

serviceName = None
serviceUser = None
serviceChan = None

serviceCtrlPack = None

videoChan = None
videoChanName = None

mouseX = -1
mouseY = -1

#mousePosChan = None
mouseClkChan = None
keyboardChan = None

mouseChrono = ElapsedTime()

monitorW = 0
monitorH = 0

import time
import argparse

args = None

parser = argparse.ArgumentParser(description="mouseGrabber")
parser.add_argument('sketchfile', help='Sketch program file')
parser.add_argument('--user', help='Flow-network username', default='User1')
parser.add_argument('--password', help='Flow-network password', default='password')
parser.add_argument('--service-name', help='The RemoteDesktop service channel', default='User1.RemoteDesktop.Ctrl')

decoder = None
f = None
fLock = threading.Lock()

app = None
guiTh = None
#kbdListener = None
wouldQuit = False
exit_flag = threading.Event()

# from here to guiTh
#framesQueue = Queue()

# from guiTh to here
mousePosQueue = Queue()
mouseClkQueue = Queue()
keyboardQueue = Queue()

def setup() -> bool:
    global args
    global parser
    global sat
    global serviceName
    global serviceUser
    global videoChanName
    global guiTh
    #global kbdListener

    args = parser.parse_args()

    sat = FlowSat() 

    #tickInterval = 0.010
    #sat.setTickTimer(tickInterval, tickInterval * 40)
    
    sat._userName = args.user
    sat._passwd = args.password

    sat.setResponseCallBack(onServiceResponse)
    sat.setNewChanCallBack(onChannelAdded)
    sat.setDelChanCallBack(onChannelRemoved)
    sat.setGrabDataCallBack(onDataGrabbed)

    serviceName = args.service_name
    serviceUser = serviceName.split(".")[0]

    #videoChanName = "{}.Screen.JPeg".format(serviceUser)
    videoChanName = "{}.Screen.PSYV".format(serviceUser)

    ok = sat.connect()

    if ok:
        guiTh = threading.Thread(target=startGui)
        guiTh.start()

        #kbdListener = keyboard.Listener(on_press=onKeyPressed, on_release=onKeyReleased)
        #kbdListener.start()

    return ok

def loop() -> bool:
    global mouseX
    global mouseY

    if mouseX > -1:
        data = struct.pack("<HH", mouseX, mouseY)
        sat.publish(mousePosChan.chanID, data)
        mouseX = -1
        mouseY = -1

    '''
    if mousePosChan is not None and not mousePosQueue.empty():
        while not mousePosQueue.empty():
            mousePos = mousePosQueue.get_nowait()
            data = struct.pack("<HH", mousePos[0], mousePos[1])
            sat.publish(mousePosChan.chanID, data)
    '''

    if mouseClkChan is not None and not mouseClkQueue.empty():
        while not mouseClkQueue.empty():
            mouseClk = mouseClkQueue.get_nowait()
            sat.publishUInt8(mouseClkChan.chanID, mouseClk)

    if keyboardChan is not None and not keyboardQueue.empty():
        while not keyboardQueue.empty():
            txt = keyboardQueue.get_nowait()
            sat.publishString(keyboardChan.chanID, txt)
        
    sat.tick()
    return sat.isConnected()

def close():
    global guiTh
    #global kbdTh
    global wouldQuit

    wouldQuit = True

    if guiTh is not None:
        guiTh.join()

    #kbdListener.stop()
    #kbdListener.join()

###############################################################################
#CALLBACKs

def onServiceResponse(chanID, response):
    global sat
    global serviceUser
    global videoChan
    global videoChanName
    global monitorW
    global monitorH

    print("Service response RECEIVED[{}]: {}".format(chanID, , response))
    
    if not response["status"]:
        print("Service HAS refused connection")
        sat.disconnect()
        return
        
    monitorW = int(response["width"])
    monitorH = int(response["height"])

    sat.addStreamingChannel(Flow_T.FT_CTRL_VALUE, Variant_T.T_UINT16, "MouseMove.CTRL", "")
    sat.addStreamingChannel(Flow_T.FT_CTRL_VALUE, Variant_T.T_UINT8, "MouseClick.CTRL", "")
    sat.addStreamingChannel(Flow_T.FT_CTRL_VALUE, Variant_T.T_INT8, "MouseScroll.CTRL", "")
    sat.addStreamingChannel(Flow_T.FT_CTRL_VALUE, Variant_T.T_STRING, "Keyboard.CTRL", "text/json")

    app.enable()

def onChannelAdded(ch):
    global sat
    global serviceName
    global serviceChan
    global serviceCtrlPack
    global videoChan
    global videoChanName
    global mousePosChan
    global mouseClkChan
    global keyboardChan
    global decoder

    if ch.name == serviceName:
        serviceChan = ch
        print("Service FOUND: {}".format(serviceChan.name))

        serviceCtrlPack = {
            "mouseMove" : "{}.MouseMove.CTRL".format(sat._userName), 
            "mouseClick" : "{}.MouseClick.CTRL".format(sat._userName), 
            "keyboard" : "{}.Keyboard.CTRL".format(sat._userName)
        }

        sat.sendServiceRequest(ch.chanID, "testCmd", serviceCtrlPack)

    elif ch.name == videoChanName:
        videoChan = ch

        sync = sat.newSyncClient()
        sync.setCurrentDbName(videoChanName)
        res = sync.getVariable("resolution")
        res = res.split("x")
        w = int(res[0])
        h = int(res[1])
        fps = sync.getVariable("fps")
        tickInterval = sync.getVariable("tickTimePeriod")
        sync.close()

        decoder = PsyvDecoder(w, h)

        print("Channel {} FOUND [ChanID: {}]".format(videoChan.name, videoChan.chanID))

        sat.setTickTimer(tickInterval, tickInterval*20)
        sat.subscribeChannel(videoChan.chanID)

    elif ch.name == "{}.MouseMove.CTRL".format(sat._userName):
        mousePosChan = ch
        print("Channel {} CREATED [ChanID: {}]".format(mousePosChan.name, mousePosChan.chanID))

    elif ch.name == "{}.MouseClick.CTRL".format(sat._userName):
        mouseClkChan = ch
        print("Channel {} CREATED [ChanID: {}]".format(mouseClkChan.name, mouseClkChan.chanID))

    elif ch.name == "{}.Keyboard.CTRL".format(sat._userName):
        keyboardChan = ch
        print("Channel {} CREATED [ChanID: {}]".format(keyboardChan.name, keyboardChan.chanID))

def onChannelRemoved(ch):
    global sat
    global serviceName
    global serviceChan
    global videoChan
    global mousePosChan
    global mouseClkChan
    global keyboardChan
    
    if ch.name == serviceName:
        print("Service CLOSED: {}".format(serviceChan.name))
        
        if videoChan is not None and sat.isSubscribed(videoChan.chanID):
            decoder = None
            sat.unsubscribeChannel(videoChan.chanID)

        if mousePosChan is not None:
            sat.removeChannel(mousePosChan.chanID)
        
        if mouseClkChan is not None:
            sat.removeChannel(mouseClkChan.chanID)
        
        if keyboardChan is not None:
            sat.removeChannel(keyboardChan.chanID)

        videoChan = None

        serviceCtrlPack = None

        mousePosChan = None
        mouseClkChan = None
        keyboardChan = None

        serviceChan = None

def onDataGrabbed(chanID, data):
    global sat
    global videoChan
    global f
    #global framQueue

    if videoChan is None:
        return

    if chanID == videoChan.chanID:
        with fLock:
            decoder.decode(data)
            f = decoder.getFrame()

        #jpg = np.frombuffer(data, np.uint8)
        #framesQueue.put(jpg)
        
###############################################################################
# pynput
''' # IT IS KEYLOGGER; IT IS NOT APROPRIATE
def onKeyPressed(key):
    global keyboardQueue

    try:
        print("Key PRESSED: (char: {})".format(key.char))
        keyboardQueue.put("{}".format(key.char))

    except AttributeError:
        print(f"Hot-key PRESSED: {key}")
        keyboardQueue.put("@{}".format(key))

def onKeyReleased(key):
    print(f"Key RELEASED: {key}")
'''
###############################################################################
# TK-GUI

def startGui():
    global app

    root = tk.Tk()
    app = App(root)
    root.mainloop()

class App:
    def __init__(self, root):
        global monitorW
        global monitorH

        self._wFactor = 0.0 
        self._hFactor = 0.0
        
        self.anchor_x = -1
        self.anchor_y = -1

        self.keySymbols = {
            'agrave'        : 'à',
            'eacute'        : 'é',
            'egrave'        : 'è',
            'igrave'        : 'ì',
            'ograve'        : 'ò',
            'ugrave'        : 'ù',
            'exclam'        : '!',
            'quotedbl'      : '"',
            'sterling'      : '£',
            'dollar'        : '$',
            'percent'       : '%',
            'ampersand'     : '&',
            'slash'         : '/',
            'parenleft'     : '(',
            'parenright'    : ')',
            'equal'         : '=',
            'question'      : '?',
            'asciicircum'   : '^',
            'apostrophe'    : '\'',
            'plus'          : 'ù',
            'minus'         : 'ù',
            'degree'        : '°',
            'section'       : '§',
            'underscore'    : '_',
            'period'        : '.',
            'colon'         : ':',
            'comma'         : ',',
            'semicolon'     : ';',
            'less'          : '<',
            'greater'       : '>',
            'ccedilla'      : 'ç',
            'backslash'     : '\\',
            'bar'           : '|',
            'braceleft'     : '{',
            'braceright'    : '}',
            'bracketleft'   : '[',
            'bracketright'  : ']',
            'numbersign'    : '#',
            'asterisk'      : '*',
            'at'            : '@'   
        }

        self.root = root
        self.root.title("Remote Desktop Flow")
        self.root.geometry("800x600")

        self.canvas = tk.Canvas(root, width=800, height=600)
        self.canvas.configure(bg="black")
        self.canvas.pack()
        
        self.root.protocol("WM_DELETE_WINDOW", self.onClose)
        self.root.bind("<Configure>", self.onResize)

        self.root.bind("<Motion>", self.onMouseMove)

        self.root.bind("<Button-1>", self.onLeftMouseClick)
        self.root.bind("<Double-Button-1>", self.onDoubleLeftMouseClick)
        self.root.bind("<ButtonRelease-1>", self.onLeftMouseRelease)
        
        self.root.bind("<Button-2>", self.onCenterMouseClick)
        self.root.bind("<Double-Button-2>", self.onDoubleCenterMouseClick)
        self.root.bind("<ButtonRelease-2>", self.onCenterMouseRelease)

        self.root.bind("<Button-3>", self.onRightMouseClick)
        self.root.bind("<Double-Button-3>", self.onDoubleRightMouseClick)
        self.root.bind("<ButtonRelease-3>", self.onRightMouseRelease)

        self.root.bind("<Button-4>", self.onMouseScrollUp)
        self.root.bind("<Button-5>", self.onMouseScrollDown)

        self.root.bind("<Key>", self.onKeyPressed)
        self.root.bind("<KeyRelease>", self.onKeyRelease)
    
        self.root.after(5, self.tick)
        
###############################################################################

    def enable(self):
        self.root.configure(cursor="none")
        self.onResize({})

###############################################################################

    def tick(self):
        global wouldQuit
        #global framesQueue
        global app

        if app and wouldQuit:
            print("Closing guiTh (wouldQuit) ..")
            self.root.quit()
            return

        self.image = None

        with fLock:
            if f is None:
                self.root.after(10, self.tick)
                return

            rgb = cv.cvtColor(f, cv.COLOR_BGR2RGB)
            self.image = Image.fromarray(rgb)
        
        '''
        if framesQueue.empty():
            self.root.after(10, self.tick)
            return

        jpg = framesQueue.get_nowait()
        self.image = Image.open(io.BytesIO(jpg))
        '''

        aspect_ratio = self.image.width / self.image.height
        
        new_width = min(self.image.width, self.canvas.winfo_width())
        new_height = int(new_width / aspect_ratio)

        if new_height > self.canvas.winfo_height():
            new_height = min(self.image.height, self.canvas.winfo_height())
            new_width = int(new_height * aspect_ratio)

        self.resizedImage = self.image.resize((new_width, new_height))

        self._wFactor = float(monitorW) / new_width
        self._hFactor = float(monitorH) / new_height

        self.anchor_x = (self.canvas.winfo_width() - new_width) / 2
        self.anchor_y = (self.canvas.winfo_height()- new_height) / 2

        self.tk_image = ImageTk.PhotoImage(self.resizedImage)
        self.canvas.create_image(self.anchor_x, self.anchor_y, anchor=tk.NW, image=self.tk_image)
        
        self.root.after(5, self.tick)

###############################################################################

    def onResize(self, event):
        global monitorW
        global monitorH

        if monitorW == 0:
            return

        w = self.root.winfo_width()
        h = self.root.winfo_height()

        self.canvas.config(width=w, height=h)
        print("RESIZE: {}x{}".format(w, h))

    def onClose(self):
        global sat
        global app
        global wouldQuit

        app = None
        print("Closed from BUTTON")

        self.root.destroy()

        if sat.isConnected():
            sat.disconnect()

###############################################################################

    def onMouseMove(self, event):
        global mouseX
        global mouseY
        
        #global mousePosQueue
        
        #if mouseChrono.stop() < 0.05:
        #    return
        
        if monitorW == 0 or self.anchor_x == -1:
            return
        
        if event.x < self.anchor_x or event.y < self.anchor_y:
            return

        if event.x-self.anchor_x > self.resizedImage.width or event.y-self.anchor_y > self.resizedImage.height:
            return

        mouseX = int((event.x-self.anchor_x) * self._wFactor) 
        mouseY = int((event.y-self.anchor_y) * self._hFactor)

        #print("Mouse MOVE ({}, {})".format(mouseX, mouseY))
        #mousePosQueue.put((mouseX, mouseY))
        

        #mouseChrono.start()

###############################################################################

    def onLeftMouseClick(self, event):
        global mouseClkQueue
        print("Mouse Left-CLICK ({}, {})".format(event.x, event.y))
        mouseClkQueue.put(0)

    def onDoubleLeftMouseClick(self, event):
        global mouseClkQueue
        print("Mouse Left-DoubleCLICK ({}, {})".format(event.x, event.y))
        mouseClkQueue.put(1)

    def onLeftMouseRelease(self, event):
        global mouseClkQueue
        print("Mouse Left-RELEASED ({}, {})".format(event.x, event.y))
        mouseClkQueue.put(2)

###############################################################################

    def onCenterMouseClick(self, event):
        global mouseClkQueue
        global mouseClkQueue
        print("Mouse Center-CLICK ({}, {})".format(event.x, event.y))
        mouseClkQueue.put(3)

    def onDoubleCenterMouseClick(self, event):
        global mouseClkQueue
        print("Mouse Center-DoubleCLICK ({}, {})".format(event.x, event.y))
        mouseClkQueue.put(4)

    def onCenterMouseRelease(self, event):
        global mouseClkQueue
        print("Mouse Center-RELEASED ({}, {})".format(event.x, event.y))
        mouseClkQueue.put(5)

###############################################################################

    def onRightMouseClick(self, event):
        global mouseClkQueue
        print("Mouse Right-CLICK ({}, {})".format(event.x, event.y))
        mouseClkQueue.put(6)

    def onDoubleRightMouseClick(self, event):
        global mouseClkQueue
        print("Mouse Right-DoubleCLICK ({}, {})".format(event.x, event.y))
        mouseClkQueue.put(7)

    def onRightMouseRelease(self, event):
        global mouseClkQueue
        print("Mouse Right-RELEASED ({}, {})".format(event.x, event.y))
        mouseClkQueue.put(8)

###############################################################################

    def onMouseScrollUp(self, event):
        global mouseClkQueue
        print("Mouse SCROLL-UP ({})".format(event.delta))
        mouseClkQueue.put(9)

    def onMouseScrollDown(self, event):
        global mouseClkQueue
        print("Mouse SCROLL-DOWN ({})".format(event.delta))
        mouseClkQueue.put(10)

###############################################################################

    def onKeyPressed(self, event):
        global keyboardQueue

        if event.char == event.keysym or event.keysym in self.keySymbols:
            print("Key TYPED: (char: {}, keysym: {})".format(event.char, event.keysym))
            keyboardQueue.put("!{}".format(event.char))

        else:
            print("Key PRESSED: (char: {}, keysym: {})".format(event.char, event.keysym))
            keyboardQueue.put("@{}".format(event.keysym))

    def onKeyRelease(self, event):
        global keyboardQueue

        if event.keysym in self.keySymbols or len(event.keysym) == 1:
            return
            
        print("Key RELEASED: (keysym: {})".format(event.keysym))
        keyboardQueue.put("#{}".format(event.keysym))

###############################################################################
